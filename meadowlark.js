var express = require('express');
var WebTorrent = require('webtorrent');



var app = express();

var fortunes = [
	"Conquer your fears or they will conquer you.",
	"Rivers nedd springs.",
	"Do not fear what you don know.",
	"You will have a pleasant surprise.",
	"Whenever possible, keep it simple.",
];
//set up handlebars view engine
var handlebars = require('express3-handlebars')
		.create({defaultLayout:'main'});
app.engine('handlebars', handlebars.engine);
app.set('view engine', 'handlebars');

app.set('port', process.env.PORT || 3000);

app.use(express.static(__dirname + '/public'));

app.get('/', function(req, res) {
	// res.type('text/plain');
	// res.send('Meadowlark Travel');

	var client = new WebTorrent()
	var magnetUri = 'magnet:?xt=urn:btih:6a9759bffd5c0af65319979fb7832189f4f3c35d'

	client.add(magnetUri, function (torrent) {
	  // Got torrent metadata!
	  console.log('Client is downloading:', torrent.infoHash)

	  torrent.files.forEach(function (file) {
	    // Display the file by appending it to the DOM. Supports video, audio, images, and
	    // more. Specify a container element (CSS selector or reference to DOM node).
	    file.appendTo('body')
	  })
	})

	res.render('home');
});

app.get('/about', function(req, res) {
	// res.type('text/plain');
	// res.send('About Meadowlark Travel');
	
	var randomFortune = fortunes[Math.floor(Math.random() * fortunes.length)];
	res.render('about', { fortune: randomFortune });
});


//custom 404 page
app.use(function(req, res) {
    // res.type('text/plain');
    // res.status(404);
    // res.send('404 - Not found');

    res.status(404);
    res.render('404');


});

//custom 500 page
app.use(function(req, res) {
    // res.type('text/plain');
    // res.status(500);
    // res.send('500 - Server error');

    console.error(err.stack);
    res.status(500);
    res.render('500');
});


app.listen(app.get('port'),function() {
	console.log( 'Express started on http://localhost:' + app.get('port') + '; press CTRL-C to terminate.')
});
