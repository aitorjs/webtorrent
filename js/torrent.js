var util = require('./util');

exports.downloadTorrent = function downloadTorrent (torrentId) {

	var client = window.client = new WebTorrent()
	
	util.log('Downloading torrent from ' + torrentId)
	
	client.add(torrentId, function(torrent) {
		console.log('Client is downloading:', torrent.infoHash)

		torrent.files.forEach(function (file) {
			file.appendTo('body')
		 })
		 
		 onTorrent (torrent)
	})

	function onTorrent (torrent) {
		var prettyBytes = require('pretty-bytes');
		
		console.log("ON TORRENT START START")
		
		util.log(
		    'Torrent info hash: ' + torrent.infoHash + ' ' +
		    '<a href="/#' + torrent.infoHash + '" onclick="prompt(\'Share this link with anyone you want to download this torrent:\', this.href);return false;">[Share link]</a> ' +
		    '<a href="' + torrent.magnetURI + '" target="_blank">[Magnet URI]</a> '
		)

		function updateSpeed () {
			console.log("Update speed")
			
		    var progress = (10 * torrent.progress).toFixed(1)
		    
		    util.updateSpeed(
		    	'<b>Peers:</b> ' + torrent.swarm.wires.length + ' ' +
		    	'<b>Progress:</b> ' + progress + '% ' +
		    	'<b>Download speed:</b> ' + prettyBytes(window.client.downloadSpeed()) + '/s ' +
		    	'<b>Upload speed:</b> ' + prettyBytes(window.client.uploadSpeed()) + '/s'
		    )
		}

		torrent.swarm.on('download', updateSpeed)
		torrent.swarm.on('upload', updateSpeed)
		setInterval(updateSpeed, 5000)
		updateSpeed()

		torrent.files.forEach(function (file) {
			// append file
		    file.appendTo(util.logElem, function (err, elem) {
		    	if (err) return error(err)
		    })

		    // append download link
		    file.getBlobURL(function (err, url) {
		    	if (err) return error(err)

		    	var a = document.createElement('a')
		    	a.target = '_blank'
		    	a.download = file.name
		    	a.href = url
		    	a.textContent = 'Download ' + file.name
		    	util.log(a)
			})
		})
	}
	
}

